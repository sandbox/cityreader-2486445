CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------
Background image formatter is used for image field. It outputs original image
as background image in the field item div wrapper and put a transparent image
with exactly the same size as a placeholder. This allows user to display a image
when they want to use 'background-size' feature of CSS in a responsive theme.


REQUIREMENTS
------------
This module requires the following modules:
 * Image in Drupal core (https://drupal.org/project/drupal)


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
* Configure user permissions in Administration » People » Permissions:

  - Access administration menu

    Users in roles with the "Access administration menu" permission will see
    the administration menu at the top of each page.

  - Display Drupal links

    Users in roles with the "Display drupal links" permission will receive
    links to drupal.org issue queues for all enabled contributed modules. The
    issue queue links appear under the administration menu icon.


* Change formatter settings in Manage display of content type.

  - Image style

    Configure image size for both original image used in background and size
    of transparent placeholder image.

  - Link image to

    Add link to placeholder image to content.


MAINTAINERS
-----------
Current maintainers:
* Eric Chen (eric.chenchao / cityreader) - https://www.drupal.org/user/265729

This project has been sponsored by:
* Monkii
  Award winning digital agency. Specialists in Digital strategy, UX design,
  Drupal. Visit http://monkii.com for more information.
